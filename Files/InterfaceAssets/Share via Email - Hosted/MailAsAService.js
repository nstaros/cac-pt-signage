/**
* @license
* Copyright © 2015 Intuilab
*
* Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
* to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
* and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
*
* The Software is provided "as is", without warranty of any kind, express or implied, including but not limited to the warranties of merchantability,
* fitness for a particular purpose and noninfringement. In no event shall the authors or copyright holders be liable for any claim, damages or other liability,
* whether in an action of contract, tort or otherwise, arising from, out of or in connection with the Software or the use or other dealings in the Software.
*
* Except as contained in this notice, the name of Intuilab shall not be used in advertising or otherwise to promote the sale,
* use or other dealings in this Software without prior written authorization from Intuilab.
*/

/**
* Inheritance on EventEmitter base class
* @type {EventEmitter}
*/
MailService.prototype = new EventEmitter();        // Here's where the inheritance occurs
MailService.prototype.constructor = MailService;

/**
* @constructor
*/
function MailService()
{
}

/**
* Send the message
* @param message the message to send
*/
MailService.prototype.send = function (displayName, to, replyTo, displayReplyTo, subject, body, attachments) {
    if (this.mailer == null)
    {
        this.mailer = intuiface.get("mailService");
        this.mailer.setMaxAttachmentSize(5 * 1024 * 1024);
    }

    var self = this;
    this.mailer.sendMessage(to, subject, body, attachments, {
        "success": function () {
            self.emit('MailSent');
			console.log ('Mail successfully sent to ' + to);
        },
        "error": function (errorMessage)
        {
                if (errorMessage == "mail too big")
                {
					self.emit('MailTooBig');
                }
                else if (errorMessage == "quota exceeded") {
                    self.emit('MailQuotaExceeded');
                }
				console.error('Mail send to ' + to + ' error: ' + errorMessage);
                self.emit('MailFailure', [errorMessage]);
        }
    }, displayName, replyTo, displayReplyTo);

    return true;
};
