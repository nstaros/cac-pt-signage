/**
 * @license
 * Copyright © 2015 Intuilab
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * The Software is provided "as is", without warranty of any kind, express or implied, including but not limited to the warranties of merchantability,
 * fitness for a particular purpose and noninfringement. In no event shall the authors or copyright holders be liable for any claim, damages or other liability,
 * whether in an action of contract, tort or otherwise, arising from, out of or in connection with the Software or the use or other dealings in the Software.
 *
 * Except as contained in this notice, the name of Intuilab shall not be used in advertising or otherwise to promote the sale,
 * use or other dealings in this Software without prior written authorization from Intuilab.
 */

WhoAmI.prototype = new EventEmitter();        // Here's where the inheritance occurs
WhoAmI.prototype.constructor = WhoAmI;

/**
 * @constructor
 */
function WhoAmI() {
    var self = this;
    // Http requests management
    this.httpService = intuiface.get("httpService");
    this.playerService = intuiface.get("playerService");

    this.whoamiServerBasePath = this.playerService.generateIntuiFaceSocialUrl("/whoami");


	var shortLicense = this.playerService.getLicenseId();
	if(shortLicense != null && shortLicense.length > 8)
	{
		shortLicense = shortLicense.substr(0,8);
	}

	this.autoPing = true;

	var deviceName = this.playerService.getDeviceName();

    var displayName;

    try {
        displayName = this.playerService.getDisplayName();
    } catch (e) {
        displayName = deviceName;
    }

    //Device info
    this.deviceInfo = {
        deviceName : deviceName,
        displayName : displayName,
        OS : this.playerService.getPlatform(),
        OSVersion : this.playerService.getOSVersion(),
        applicationName : this.playerService.getApplicationName(),
        completeApplicationName : this.playerService.getCompleteApplicationName(),
        experienceName: this.playerService.getCurrentExperienceName(),
        intuifaceVersion : this.playerService.getIntuiFaceVersion(),
		intuifaceLicense : shortLicense
    };

    //network info
    this.network = {
        ipAddress : "",
        isOnline : false
    };

    //location info
    this.location = {
        country:"",
        city:"",
        latitude:0,
        longitude:0
    };

    this.timerFrequency = 60000;

    this.initializing = true;
    this.refreshInfo();

}

WhoAmI.prototype.setAutoPing = function (autoPing) {

    if (this.autoPing != autoPing) {

        this.autoPing = autoPing;
        this.emit('autoPingChanged', [this.autoPing]);

    }
};

WhoAmI.prototype.setIsOnline = function (isOnline) {
    var self = this;
    if (this.network.isOnline != isOnline) {

        this.network.isOnline = isOnline;
        self.emit('networkChanged', [self.network]);
        if(!this.initializing)
        {
            if (isOnline){
                self.emit('online');
            }
            else{
                self.emit('offline');
            }
        }
    }
};


WhoAmI.prototype.refreshInfo = function() {
    var self = this;

    //Clear interval to stop the timer
    clearInterval(this._timerID);

    //Call the Ping function (with IP and Location Info option)
    this._ping(true);

    //Restart timer
    this._timerID = setInterval(function () {
        self._autoPing();
    }, this.timerFrequency);
};

WhoAmI.prototype._ping = function(withIpAndLocationInfo) {

    var self = this;
    this.httpService.get("https://data.intuilab.com/CheckOnlineStatus.html?t=0" + Date.now(), {
        "success": function(result) {
            //process result
            self.setIsOnline(true);
            self.emit('networkChanged', [self.network]);
            if (withIpAndLocationInfo)
            {
                self._getIp();
            }
            self.initializing = false;
        },
        "error" : function(errorMessage) {
            //TODO: handle error
            //self.emit('Error', [errorMessage]);
            //self.network.ipAddress = "";
            self.setIsOnline(false);
            self.emit('networkChanged', [self.network]);
            self.initializing = false;
        }});


};

WhoAmI.prototype._autoPing = function() {

    //Test if "autoPing" is enabled
    if (this.autoPing){
        //Call the Ping function (without IP and Location Info option)
        this._ping(false);
    }

};


WhoAmI.prototype._getIp = function() {

    var self = this;

    if (this.network.isOnline)
    {
        this.httpService.get(this.whoamiServerBasePath + "/ip", {
            "success": function(result) {
                //process result
                self.network.ipAddress = result;
                self.emit('networkChanged', [self.network]);
                self._getLocationInfo();
            },
            "error" : function(errorMessage) {
                //TODO: handle error
                //self.emit('Error', [errorMessage]);
                //self.network.ipAddress = "";
                //self.emit('networkChanged', [self.network]);
            }});
    }

};


WhoAmI.prototype._getLocationInfo = function() {

    var self = this;

    if (this.network.isOnline)
    {
        if (this.network.ipAddress != "") {
            var request = this.whoamiServerBasePath + "/location.json";

            this.httpService.get(request, {
                "success": function(result) {
                    //process result
                    var obj = result;
                    if (typeof obj != 'object') {
                        obj = JSON.parse(result);
                    }

                    self.location.country = obj.country;
                    self.location.city = obj.city;
                    self.location.latitude = obj.lat;
                    self.location.longitude = obj.lon;
                    self.emit('locationChanged', [self.location]);
                },
                "error" : function(errorMessage) {
                    //TODO: handle error
                    //self.emit('Error', [errorMessage]);
                }});
        }
    }

};


/**
 *
 */
WhoAmI.prototype.dispose = function() {

    // Stop timer
    if (this._timerID != null)
    {
        clearInterval(this._timerID);
        this._timerID = null;
    }

};
