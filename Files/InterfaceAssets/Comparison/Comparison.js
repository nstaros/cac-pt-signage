/**
 * @license
 * Copyright © 2015 Intuilab
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * The Software is provided "as is", without warranty of any kind, express or implied, including but not limited to the warranties of merchantability,
 * fitness for a particular purpose and noninfringement. In no event shall the authors or copyright holders be liable for any claim, damages or other liability,
 * whether in an action of contract, tort or otherwise, arising from, out of or in connection with the Software or the use or other dealings in the Software.
 *
 * Except as contained in this notice, the name of Intuilab shall not be used in advertising or otherwise to promote the sale,
 * use or other dealings in this Software without prior written authorization from Intuilab.
 */

/**
* Inheritance on EventEmitter base class
* @type {EventEmitter}
*/
Comparison.prototype = new EventEmitter();        // Here's where the inheritance occurs
Comparison.prototype.constructor = Comparison;

/**
 * Function to convert a string to boolean.
 */
function parseBool( myString )
{
	if ( typeof myString == "boolean" )
	{
		return myString;
	}

	return typeof myString === 'string' && myString.toLowerCase() == 'true';
}

/**
* @constructor
*/
function Comparison() {

    this.inputA = "";
    this.inputB = "";
    this.operator = 'Equal';
    this.output = "";
}

/**
 * Setters for different properties
 */
Comparison.prototype.setInputA = function (values) {
    if (this.inputA !== values) {
        this.inputA = values;
        this.emit('inputAChanged', [this.inputA]);

        this.evaluate();
    }
};

Comparison.prototype.setInputB = function (values) {
    if (this.inputB !== values) {
        this.inputB = values;
        this.emit('inputBChanged', [this.inputB]);

        this.evaluate();
    }
};

Comparison.prototype.setOperator = function (values) {
    if (this.separator !== values) {
        this.operator = values;
        this.emit('operatorChanged', [this.operator]);

        this.evaluate();
    }
};

Comparison.prototype.setInputAInputB = function (InputA, InputB) {
    if ((this.inputA !== InputA) || (this.inputB !== InputB)){
        if (this.inputA !== InputA){
            this.inputA = InputA;
            this.emit('inputAChanged', [this.inputA]);
        }
        if (this.inputB !== InputB){
            this.inputB = InputB;
            this.emit('inputBChanged', [this.inputB]);
        }
        this.evaluate();
    }
};

/**
 *
 *
 */
Comparison.prototype.evaluate = function () {

    var newResult = this.test();
    if (this.output != newResult)
    {
        this.output = newResult;
        this.emit('outputChanged', [this.output]);
        if (this.output == true)
        {
            this.emit('outputIsTrue');
        }
        else
        {
            this.emit('outputIsFalse');
        }
    }

};



/**
 *
 *
 */
Comparison.prototype.test = function () {

// then according to operator, check if the condition has been fulfilled or not
    switch (this.operator)
    {
        case "Equal":
            if ( typeof this.inputA == "boolean" || typeof this.inputB == "boolean" )
            {
                return parseBool(this.inputA) == parseBool(this.inputB);
            }
            else if ( typeof this.inputA == "number" || typeof this.inputB == "number" )
            {
                return parseFloat(this.inputA) == parseFloat(this.inputB);
            }
            else
            {
                return this.inputA == this.inputB;
            }
            break;
        case "NotEqual":
            if ( typeof this.inputA == "boolean" || typeof this.inputB == "boolean" )
            {
                return parseBool(this.inputA) != parseBool(this.inputB);
            }
            else if ( typeof this.inputA == "number" || typeof this.inputB == "number" )
            {
                return parseFloat(this.inputA) != parseFloat(this.inputB);
            }
            else
            {
                return this.inputA != this.inputB;
            }
            break;
        case "LessThan":
            var A = parseFloat(this.inputA);
            var B = parseFloat(this.inputB);

            if (isNaN(A) || isNaN(B))
            {
                return this.inputA < this.inputB;
            }
            else
            {
                if ((A.toString() == (this.inputA).toString()) && (B == (this.inputB).toString()))
                {
                    return A < B;
                }
                else
                {
                    return this.inputA < this.inputB;
                }
            }
            break;
        case "LessThanOrEqual":
            var A = parseFloat(this.inputA);
            var B = parseFloat(this.inputB);

            if (isNaN(A) || isNaN(B))
            {
                return this.inputA <= this.inputB;
            }
            else
            {
                if ((A.toString() == (this.inputA).toString()) && (B == (this.inputB).toString()))
                {
                    return A <= B;
                }
                else
                {
                    return this.inputA <= this.inputB;
                }
            }
            break;
        case "GreaterThan":
            var A = parseFloat(this.inputA);
            var B = parseFloat(this.inputB);

            if (isNaN(A) || isNaN(B))
            {
                return this.inputA > this.inputB;
            }
            else
            {
                if ((A.toString() == (this.inputA).toString()) && (B == (this.inputB).toString()))
                {
                    return A > B;
                }
                else
                {
                    return this.inputA > this.inputB;
                }
            }
            break;
        case "GreaterThanOrEqual":
            var A = parseFloat(this.inputA);
            var B = parseFloat(this.inputB);

            if (isNaN(A) || isNaN(B))
            {
                return this.inputA >= this.inputB;
            }
            else
            {
                if ((A.toString() == (this.inputA).toString()) && (B == (this.inputB).toString()))
                {
                    return A >= B;
                }
                else
                {
                    return this.inputA >= this.inputB;
                }
            }
            break;
            break;
        case "StartsWith":
            if ( this.inputA == null || this.inputB == null || typeof this.inputA !== "string" || typeof this.inputB !== "string" )
            {
                return false;
            }
            return this.inputA.indexOf(this.inputB) == 0;
            break;
        case "DoesNotStartWith":
            if ( this.inputA == null || this.inputB == null || typeof this.inputA !== "string" || typeof this.inputB !== "string" )
            {
                return true;
            }
            return !(this.inputA.indexOf(this.inputB) == 0);
            break;
        case "EndsWith":
            if ( this.inputA == null || this.inputB == null || typeof this.inputA !== "string" || typeof this.inputB !== "string" )
            {
                return false;
            }
            return this.inputA.indexOf(this.inputB, this.inputA.length - this.inputB.length) !== -1;
            break;
        case "DoesNotEndWith":
            if ( this.inputA == null || this.inputB == null || typeof this.inputA !== "string" || typeof this.inputB !== "string" )
            {
                return true;
            }
            return !(this.inputA.indexOf(this.inputB, this.inputA.length - this.inputB.length) !== -1);
            break;
        case "Contains":
            if ( this.inputA == null || this.inputB == null || typeof this.inputA !== "string" || typeof this.inputB !== "string" )
            {
                return false;
            }
            return this.inputA.indexOf(this.inputB) > -1;
            break;
        case "DoesNotContain":
            if ( this.inputA == null || this.inputB == null || typeof this.inputA !== "string" || typeof this.inputB !== "string" )
            {
                return true;
            }
            return this.inputA.indexOf(this.inputB) == -1;
            break;
        case "Matches":
            if ( this.inputA == null || this.inputB == null || typeof this.inputA !== "string" || typeof this.inputB !== "string" )
            {
                return true;
            }
            var regExp = new RegExp(this.inputB);
            return regExp.test(this.inputA);
            break;
        case "DoesNotMatch":
            if ( this.inputA == null || this.inputB == null || typeof this.inputA !== "string" || typeof this.inputB !== "string" )
            {
                return true;
            }
            var regExp = new RegExp(this.inputB);
            return !regExp.test(this.inputA);
            break;
    }

};



