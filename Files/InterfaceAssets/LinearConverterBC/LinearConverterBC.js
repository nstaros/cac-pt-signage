/**
* @license
* Copyright © 2015 Intuilab
*
* Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
* to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
* and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
* 
* The Software is provided "as is", without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, 
* fitness for a particular purpose and noninfringement. In no event shall the authors or copyright holders be liable for any claim, damages or other liability, 
* whether in an action of contract, tort or otherwise, arising from, out of or in connection with the Software or the use or other dealings in the Software.
* 
* Except as contained in this notice, the name of Intuilab shall not be used in advertising or otherwise to promote the sale, 
* use or other dealings in this Software without prior written authorization from Intuilab.
*/

/**
* Inheritance on EventEmitter base class
* @type {EventEmitter}
*/
LinearConverterBC.prototype = new Object();          // Here's where the inheritance occurs
LinearConverterBC.prototype.constructor = LinearConverterBC;

/**
* @constructor
*/
function LinearConverterBC() {

}


/**
 * @method
 */
LinearConverterBC.prototype.linearConverter = function (input, inputMin, outputMin, inputMax, outputMax, limitMin, limitMax) {


    try{

        if (input == null || inputMin == null || inputMax == null || outputMin == null || outputMax == null || limitMin == null || limitMax == null) return null;

        if (isNaN(input) || isNaN(inputMin) || isNaN(inputMax) || isNaN(outputMin) || isNaN(outputMax)) {
            return null;
        }
        else{
            var tempValue = (parseFloat(input) - parseFloat(inputMin)) / (parseFloat(inputMax) - parseFloat(inputMin));
            var newOutput = ((parseFloat(outputMax) - parseFloat(outputMin)) * tempValue) + parseFloat(outputMin);
        }

        if ((limitMin == true) && (newOutput < Math.min(outputMin, outputMax)) ){
            return Math.min(outputMin, outputMax);
        }
        else if ((limitMax == true) && (newOutput > Math.max(outputMin, outputMax))){
            return Math.max(outputMin, outputMax);
        }
        else{
            return newOutput;
        }

    }
    catch (e){
        return null;
    }


};