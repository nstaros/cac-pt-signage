/**
 * @license
 * Copyright © 2015 Intuilab
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * The Software is provided "as is", without warranty of any kind, express or implied, including but not limited to the warranties of merchantability,
 * fitness for a particular purpose and noninfringement. In no event shall the authors or copyright holders be liable for any claim, damages or other liability,
 * whether in an action of contract, tort or otherwise, arising from, out of or in connection with the Software or the use or other dealings in the Software.
 *
 * Except as contained in this notice, the name of Intuilab shall not be used in advertising or otherwise to promote the sale,
 * use or other dealings in this Software without prior written authorization from Intuilab.
 */


TextManipulation.prototype = new EventEmitter();

function TextManipulation() {
    this.text = "";
    this.output = "";
    this.internalSeparator = " ";
    this.defineEvents(['textChanged']);
}

/**
 * Set the text IA property
 * @param {String} newText
 */
TextManipulation.prototype.setText = function (newText) {
    this.text = newText;
    this.emit('textChanged', [newText]);
};

/**
 * Change text and output with a new text if overwrite is true, only change output otherwise
 * @param {String|Boolean} newText
 * @param {Boolean} overwrite
 * @private
 */
TextManipulation.prototype._changeProperties = function (newText, overwrite) {
    this.output = newText;
    this.emit('outputChanged', [newText]);
    if (overwrite) {
        this.text = newText;
        this.emit('textChanged', [newText]);
    }
};

/**
 * Concatenate a text to the IA text
 * @param {String} text
 * @param {Boolean} overwrite
 */
TextManipulation.prototype.concatenate = function (text, overwrite) {
    var concatenation = this.text + text;
    this._changeProperties(concatenation, overwrite);
};

/**
 * Replace a text from the IA text with another one
 * @param {String} oldText
 * @param {String} newText
 * @param {Boolean} overwrite
 */
TextManipulation.prototype.replace = function (oldText, newText, overwrite) {
    var regex = new RegExp(oldText, "g");
    var replace = this.text.replace(regex, newText);
    this._changeProperties(replace, overwrite);
};

/**
 * Extract a substring between two indexes from the IA text
 * @param {Number} startIndex 1 to x
 * @param {Number} endIndex   1 to x
 * @param {Boolean} overwrite
 */
TextManipulation.prototype.substring = function (startIndex, endIndex, overwrite) {
    var substring = this.text.substring(startIndex - 1, endIndex);
    this._changeProperties(substring, overwrite);
};

/**
 * Split the IA text according to a separator and return the 'index'th item of the split result
 * @param {String} separator
 * @param {Number} index 1 to x
 * @param {Boolean} overwrite
 */
TextManipulation.prototype.itemAt = function (separator, index, overwrite) {

    switch (separator) {
        case "blank":
            this.internalSeparator = " ";
            break;
        case "comma":
            this.internalSeparator = ",";
            break;
        case "semicolon":
            this.internalSeparator = ";";
            break;
        case "dash":
            this.internalSeparator = "-";
            break;
        case "pipe":
            this.internalSeparator = "|";
            break;
        case "underscore":
            this.internalSeparator = "_";
            break;
    }

    var split = this.text.split(this.internalSeparator)[index - 1];
    this._changeProperties(split, overwrite);
};

/**
 * Clear both text and output IA properties
 */
TextManipulation.prototype.clear = function () {
    var clear = "";
    this._changeProperties(clear, true);
};


/**
 * Convert the string to lowercase letters
 * @param {Boolean} overwrite
 */
TextManipulation.prototype.toLowerCase = function (overwrite) {
    var lowerCase = this.text.toLowerCase();
    this._changeProperties(lowerCase, overwrite);
};

/**
 * Convert the string to an uppercase pattern
 * @param {String} pattern
 * @param {Boolean} overwrite
 */
TextManipulation.prototype.toUpperCase = function (pattern, overwrite) {
    switch (pattern) {
        case "all":
            this._fullUpperCase(overwrite);
            break;
        case "firstLetter":
            this._firstLetterUppercase(overwrite);
            break;
        case "properName":
            this._properName(overwrite);
            break;
        default :
            break;
    }
};

/**
 * Convert the string to uppercase letters
 * @param {Boolean} overwrite
 * @private
 */
TextManipulation.prototype._fullUpperCase = function (overwrite) {
    var upperCase = this.text.toUpperCase();
    this._changeProperties(upperCase, overwrite);
};

/**
 * Convert only the first letter to uppercase
 * @param overwrite
 * @private
 */
TextManipulation.prototype._firstLetterUppercase = function (overwrite) {
    var firstChar = this.text.charAt(0);
    var firstCharToUpperCase = firstChar.toUpperCase();
    var textLessFirstChar = this.text.substring(1);
    var textLessFirstCharToLowerCase = textLessFirstChar.toLowerCase();
    var properName = firstCharToUpperCase + textLessFirstCharToLowerCase;
    this._changeProperties(properName, overwrite);
};

/**
 * Convert first letter of every word to uppercase
 * @param overwrite
 * @private
 */
TextManipulation.prototype._properName = function (overwrite) {
    var words = this.text.split(" ");
    var result = "";
    for (var i = 0; i < words.length; i++) {
        var temp = words[i].toLowerCase();
        temp = temp.charAt(0).toUpperCase() + temp.substring(1);
        result = result + temp;
        if (i < words.length - 1) {
            result = result + " ";
        }
    }
    this._changeProperties(result, overwrite);
};